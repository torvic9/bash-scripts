#!/bin/bash
# overcomplicated password generator
# using urandom, openssl and argon2
# by torvic9

[[ ! -f /usr/bin/argon2 ]] && echo "Please install argon2!" && exit 1

if [[ -z $1 ]] ; then
    echo -e "No password length given, using default of 12!"
    echo -e "Usage: './pwgen.sh <password-length>'\n"
    _leng=12
elif [[ $1 -gt 32 || $1 -lt 1 ]] ; then
    _leng=32
else
    _leng=$1
fi

# parameters to argon2
_argon='-id -e -l 48 -k 65536 -t 6'

for ((i=1 ; i<=5 ; i++)) ; do
    _rnd=$(od -A n -t uI -N 4 /dev/urandom | tr -d ' \n')
    _offset=$(( ($_rnd % 3) + ($_rnd % 5) + ($_rnd % 11) ))
    [ $_offset -eq 0 ] && _offset=1
    _salt=$(openssl rand -hex 8)
    _urand=$(head -c 32 </dev/urandom | tr -d '\0')
    argon2 $_salt $_argon <<<$_urand | awk '{print substr($1,55)}' | \
      cut -c$_offset-$(( $_offset + $_leng - 1 ))
done

exit 0
