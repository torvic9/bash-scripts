#!/bin/bash
# CompComp: compare compression
# Usage: compcomp.sh --<compression-algo> <testfile>
# by torvic9

# help
HELP() {
    echo "Usage: compcomp.sh --<comp-algo> <testfile>"
    echo "<comp-algo> is one of zstd, xz, gzip, lz4, bzip3 or all"
    exit 0
}

# zstd
ZSTD() {
    local comp='zstd'
    echo "----- $comp -----"
    echo -e "Filename: $filename\t Original file size: $filesize\n" >> $results
    for i in {3,6,10,14,19} ; do
        echo "${comp}-${i}: " >> $results
        /usr/bin/time -f%E -a -o $results ${comp} -k -T${cores} -${i} $filename
        cfilesize=`ls -s $filename.zst | awk '{ printf("%lu kilobytes\n",$1) }'`
        echo -e "$cfilesize\n" >> $results
        rm $filename.zst && sync
    done
}

# xz
XZ() {
    local comp='xz'
    echo "----- $comp -----"
    echo -e "Filename: $filename\t Original file size: $filesize\n" >> $results
    for i in {1,4,6} ; do
        echo "${comp}-${i}: " >> $results
        /usr/bin/time -f%E -a -o $results ${comp} -k -v -T${cores} -${i} $filename
        cfilesize=`ls -s $filename.xz | awk '{ printf("%lu kilobytes\n",$1) }'`
        echo -e "$cfilesize\n" >> $results
        rm $filename.xz && sync
    done
}

# gzip
GZIP() {
    local comp='gzip'
    echo "----- $comp -----"
    echo -e "Filename: $filename\t Original file size: $filesize\n" >> $results
    for i in {6,9} ; do
        echo "${comp}-${i}: " >> $results
        /usr/bin/time -f%E -a -o $results ${comp} -k -v -${i} $filename
        cfilesize=`ls -s $filename.gz | awk '{ printf("%lu kilobytes\n",$1) }'`
        echo -e "$cfilesize\n" >> $results
        rm $filename.gz && sync
    done
}

# lz4
LZ4() {
    local comp='lz4'
    echo "----- $comp -----"
    echo -e "Filename: $filename\t Original file size: $filesize\n" >> $results
    for i in {1,3} ; do
        echo "${comp}-${i}: " >> $results
        /usr/bin/time -f%E -a -o $results ${comp} -k -v -${i} $filename
        cfilesize=`ls -s $filename.lz4 | awk '{ printf("%lu kilobytes\n",$1) }'`
        echo -e "$cfilesize\n" >> $results
        rm $filename.lz4 && sync
    done
}

# bzip3
BZIP3() {
    local comp='bzip3'
    echo "----- $comp -----"
    echo -e "Filename: $filename\t Original file size: $filesize\n" >> $results
    for i in {8,16,32} ; do
        echo "${comp}-${i}: " >> $results
        /usr/bin/time -f%E -a -o $results ${comp} -j ${cores} -v -b ${i} $filename $filename.bz3
        cfilesize=`ls -s $filename.bz3 | awk '{ printf("%lu kilobytes\n",$1) }'`
        echo -e "$cfilesize\n" >> $results
        rm $filename.bz3 && sync
    done
}

# run
[[ -z $# ]] && HELP
[[ ! -f $2 ]] && echo "No filename provided or file does not exist, aborting!" && exit 1

cores=`nproc`
algos=('ZSTD' 'XZ' 'GZIP' 'LZ4' 'BZIP3')
filename=$2
results="compcomp-$(basename "${filename}").log"
filesize=`ls -s $filename | awk '{ printf("%lu kilobytes\n",$1) }'`
echo -e "Filename: $filename\t Original file size: $filesize\n"

case "$1" in
    --zstd)   ${algos[0]} ;;
    --xz)     ${algos[1]} ;;
    --gzip)   ${algos[2]} ;;
    --lz4)    ${algos[3]} ;;
    --bzip3)  ${algos[4]} ;;
    --all)    for a in "${algos[@]}" ; do $a ; done ;;
    *)        HELP ;;
esac

exit 0
